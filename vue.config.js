module.exports = {
    devServer: {
        host: 'localhost',//target host
        port: 8082,
        proxy: {
            '/api': {
                target: 'http://server-cloud-test.bytebase.cn/cloudAdmin',
                changeOrigin: true,
                ws: true,
                autoRewrite: true,
                cookieDomainRewrite: true,
                pathRewrite: {
                    '^/api':''
                }
            },
            '/minio': {
                target: 'http://bos-test.bytebase.cn',
                changeOrigin: true,
                ws: true,
                autoRewrite: true,
                cookieDomainRewrite: true,
                pathRewrite: {
                    '^/minio':''
                }
            }
        }
    }
}
