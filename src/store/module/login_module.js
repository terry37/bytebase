const login = {
    state: {
        testData: '我是从store中获取的数据',
        num: 1,
    },
    getters: {
        testData: (state) => state.testData
    },
    actions: {
        getOrderQueryList({ commit, state}) {
            setTimeout(() => {
                let num = state.num+1
                commit('changeNum',num)
            }, 3)
        }
    },
    mutations: {
        changeNum(state, value) {
            state.num = value
        },
    },
}
export default login
