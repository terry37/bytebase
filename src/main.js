import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios' // 配置axios
import store from './store'
import { List, PullRefresh, Search, NavBar, Toast, Lazyload, Swipe, SwipeItem, Popover, Form, Field, Button, Icon, Image as VanImage } from 'vant'

Vue.use(List)
Vue.use(PullRefresh)
Vue.use(Search)
Vue.use(NavBar)
Vue.use(Toast)
Vue.use(Lazyload)
Vue.use(Lazyload, {
  lazyComponent: true
})
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Popover)
Vue.use(Form)
Vue.use(Field)
Vue.use(Button)
Vue.use(Icon)
Vue.use(VanImage)

Vue.prototype.$axios = Axios
Vue.config.productionTip = false

new Vue({
  Axios,
  store,
  render: h => h(App),
}).$mount('#app')
