//token.js

import Cookies from 'js-cookie'

const key = 'TOKEN'

/*
 * 获取Token
*/
export function getToken() {
    return Cookies.get(key)
}

/*
 * 设置Token
*/
export function setToken(token) {
    return Cookies.set(key, token)
}

/*
 * 移除Token
*/
export function removeToken() {
    return Cookies.remove(key)
}
